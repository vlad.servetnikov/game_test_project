import 'package:equatable/equatable.dart';

abstract class GameEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class FetchGames extends GameEvent {
 final int offset;

  FetchGames({this.offset});
}