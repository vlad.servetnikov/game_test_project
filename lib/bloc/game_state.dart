import 'package:equatable/equatable.dart';
import 'package:game_project/models/game_model.dart';

enum GameStatus { initial, success, failure }

// ignore: must_be_immutable
class GameState extends Equatable {
   GameState({
    this.status = GameStatus.initial,
    this.withInternet = true,
    this.games = const <GameModel>[],
    this.hasReachedMax = false,
  });

  final GameStatus status;
  List<GameModel> games;
  final bool hasReachedMax;
  final bool withInternet;

  GameState copyWith(
      {GameStatus status,
      List<GameModel> games,
      bool hasReachedMax,
      bool withInternet}) {
    return GameState(
        status: status ?? this.status,
        games: games ?? this.games,
        hasReachedMax: hasReachedMax ?? this.hasReachedMax,
        withInternet: withInternet ?? this.withInternet);
  }

  @override
  String toString() {
    return '''GameState { status: $status, hasReachedMax: $hasReachedMax, withInternet: $withInternet games: ${games.length} }''';
  }

  @override
  List<Object> get props => [status, games, hasReachedMax, withInternet];
}
