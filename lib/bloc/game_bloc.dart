import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:game_project/app/locator.dart';
import 'package:game_project/bloc/game_event.dart';
import 'package:game_project/bloc/game_state.dart';
import 'package:game_project/db/app_database.dart';
import 'package:game_project/db/dao/game_dao.dart';
import 'package:game_project/models/game_model.dart';
import 'package:game_project/models/game_response.dart';
import 'package:game_project/service/game_repository.dart';
import 'package:rxdart/rxdart.dart';

class GameBloc extends Bloc<GameEvent, GameState> {
  GameBloc({@required this.gamesRepository}) : super(GameState());

  final GamesRepository gamesRepository;

  var gameRepository = GamesRepository();
  GameDao gameDao = locator<AppDatabase>().gameDao;

  @override
  Stream<Transition<GameEvent, GameState>> transformEvents(
    Stream<GameEvent> events,
    TransitionFunction<GameEvent, GameState> transitionFn,
  ) {
    return super.transformEvents(
      events.debounceTime(const Duration(milliseconds: 500)),
      transitionFn,
    );
  }

  @override
  Stream<GameState> mapEventToState(GameEvent event) async* {
    if (event is FetchGames) {
      yield await _mapPostFetchedToState(state, event);
    }
  }

  // ignore: missing_return
  Future<GameState> _mapPostFetchedToState(
      GameState state, FetchGames event) async {
    if (state.hasReachedMax) return state;
    try {
      if (state.status == GameStatus.initial) {
        final GameResponse resp = await gamesRepository.getAllGames(offset: 0);
        for (GameModel game in resp.games) {
          await gameDao.insertGame(game);
        }
        return state.copyWith(
          status: GameStatus.success,
          games: resp.games,
          hasReachedMax: false,
          withInternet: true,
        );
      }
      final GameResponse response = await gamesRepository.getAllGames(
          offset: event.offset ?? state.games.length);
      for (GameModel game in response.games) {
        await gameDao.insertGame(game);
      }
      if(event.offset == 0){
        state.games = [];
      }
      return response.games.isEmpty
          ? state.copyWith(hasReachedMax: true)
          : state.copyWith(
              status: GameStatus.success,
              games: List.of(state.games)..addAll(response.games),
              hasReachedMax: false,
              withInternet: true,
            );
    } catch (_) {
      state.games = [];
      List<GameModel> games = await gameDao.fetchGames();
      return state.copyWith(
          status: GameStatus.success,
          hasReachedMax: false,
          withInternet: false,
          games: List.of(state.games)..addAll(games));
    }
  }
}
