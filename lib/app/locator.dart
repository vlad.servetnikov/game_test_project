import 'package:game_project/db/app_database.dart';
import 'package:get_it/get_it.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  setupDatabaseLocator();
}

void setupDatabaseLocator() {
  locator.registerSingleton<AppDatabase>(AppDatabase());
}
