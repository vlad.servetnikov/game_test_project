import 'package:game_project/models/game_response.dart';
import 'package:game_project/service/game_api_provider.dart';

class GamesRepository {
  GameProvider _gameProvider = GameProvider();

  Future<GameResponse> getAllGames({int offset}) =>
      _gameProvider.getGames(offset);
}
