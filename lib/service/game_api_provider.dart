import 'package:dio/dio.dart';
import 'package:dio_logger/dio_logger.dart';
import 'package:game_project/models/game_response.dart';

const _postLimit = 10;

class GameProvider {

  var url = 'https://api.igdb.com/v4/games';

  String dataGame = "fields artworks,bundles,checksum,collection,cover.*,created_at,first_release_date,follows,game_engines.*,hypes,multiplayer_modes,name,parent_game, player_perspectives.*,rating,rating_count,screenshots.*,slug,standalone_expansions,status,storyline,summary,total_rating,total_rating_count,updated_at,url,version_parent,version_title,videos.*; sort popularity desc;";

  static BaseOptions options = BaseOptions(
    headers: <String, String>{
      'Client-ID': 'ikefu3gjaojsnnt21ik7orxyofnztq',
      'Authorization': 'Bearer 40006fkjt3awoz3nduflzs86rq6d7k'
    },
  );

  final dio = Dio(options)
    ..interceptors.add(dioLoggerInterceptor)
    ..get("https://api.igdb.com/v4/games");

  // ignore: missing_return
  Future<GameResponse> getGames([int offset = 0]) async {
    try {
      Response response = await dio.post(url,
          data: '$dataGame limit: $_postLimit; offset: $offset;');
      var gameResponse = GameResponse.fromJson(response.data);
      return gameResponse;
    } on DioError catch (e) {
      print('DioError ${e.message}');
    } catch (e) {
      print('unknown error: $e');
    }
  }
}
