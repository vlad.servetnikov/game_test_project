// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_database.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps, unnecessary_this
class GameTableData extends DataClass implements Insertable<GameTableData> {
  final int id;
  final int createdAt;
  final int firstRelease;
  final double popularity;
  final String summary;
  final double rating;
  final String name;
  final bool alphaChannel;
  final bool animated;
  final int game;
  final int height;
  final String imageId;
  final String url;
  final int width;
  final String checksum;
  GameTableData(
      {@required this.id,
      @required this.createdAt,
      @required this.firstRelease,
      @required this.popularity,
      @required this.summary,
      @required this.rating,
      @required this.name,
      @required this.alphaChannel,
      @required this.animated,
      @required this.game,
      @required this.height,
      @required this.imageId,
      @required this.url,
      @required this.width,
      @required this.checksum});
  factory GameTableData.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final doubleType = db.typeSystem.forDartType<double>();
    final stringType = db.typeSystem.forDartType<String>();
    final boolType = db.typeSystem.forDartType<bool>();
    return GameTableData(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      createdAt:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
      firstRelease: intType
          .mapFromDatabaseResponse(data['${effectivePrefix}first_release']),
      popularity: doubleType
          .mapFromDatabaseResponse(data['${effectivePrefix}popularity']),
      summary:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}summary']),
      rating:
          doubleType.mapFromDatabaseResponse(data['${effectivePrefix}rating']),
      name: stringType.mapFromDatabaseResponse(data['${effectivePrefix}name']),
      alphaChannel: boolType
          .mapFromDatabaseResponse(data['${effectivePrefix}alpha_channel']),
      animated:
          boolType.mapFromDatabaseResponse(data['${effectivePrefix}animated']),
      game: intType.mapFromDatabaseResponse(data['${effectivePrefix}game']),
      height: intType.mapFromDatabaseResponse(data['${effectivePrefix}height']),
      imageId: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}image_id']),
      url: stringType.mapFromDatabaseResponse(data['${effectivePrefix}url']),
      width: intType.mapFromDatabaseResponse(data['${effectivePrefix}width']),
      checksum: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}checksum']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || createdAt != null) {
      map['created_at'] = Variable<int>(createdAt);
    }
    if (!nullToAbsent || firstRelease != null) {
      map['first_release'] = Variable<int>(firstRelease);
    }
    if (!nullToAbsent || popularity != null) {
      map['popularity'] = Variable<double>(popularity);
    }
    if (!nullToAbsent || summary != null) {
      map['summary'] = Variable<String>(summary);
    }
    if (!nullToAbsent || rating != null) {
      map['rating'] = Variable<double>(rating);
    }
    if (!nullToAbsent || name != null) {
      map['name'] = Variable<String>(name);
    }
    if (!nullToAbsent || alphaChannel != null) {
      map['alpha_channel'] = Variable<bool>(alphaChannel);
    }
    if (!nullToAbsent || animated != null) {
      map['animated'] = Variable<bool>(animated);
    }
    if (!nullToAbsent || game != null) {
      map['game'] = Variable<int>(game);
    }
    if (!nullToAbsent || height != null) {
      map['height'] = Variable<int>(height);
    }
    if (!nullToAbsent || imageId != null) {
      map['image_id'] = Variable<String>(imageId);
    }
    if (!nullToAbsent || url != null) {
      map['url'] = Variable<String>(url);
    }
    if (!nullToAbsent || width != null) {
      map['width'] = Variable<int>(width);
    }
    if (!nullToAbsent || checksum != null) {
      map['checksum'] = Variable<String>(checksum);
    }
    return map;
  }

  GameTableCompanion toCompanion(bool nullToAbsent) {
    return GameTableCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      createdAt: createdAt == null && nullToAbsent
          ? const Value.absent()
          : Value(createdAt),
      firstRelease: firstRelease == null && nullToAbsent
          ? const Value.absent()
          : Value(firstRelease),
      popularity: popularity == null && nullToAbsent
          ? const Value.absent()
          : Value(popularity),
      summary: summary == null && nullToAbsent
          ? const Value.absent()
          : Value(summary),
      rating:
          rating == null && nullToAbsent ? const Value.absent() : Value(rating),
      name: name == null && nullToAbsent ? const Value.absent() : Value(name),
      alphaChannel: alphaChannel == null && nullToAbsent
          ? const Value.absent()
          : Value(alphaChannel),
      animated: animated == null && nullToAbsent
          ? const Value.absent()
          : Value(animated),
      game: game == null && nullToAbsent ? const Value.absent() : Value(game),
      height:
          height == null && nullToAbsent ? const Value.absent() : Value(height),
      imageId: imageId == null && nullToAbsent
          ? const Value.absent()
          : Value(imageId),
      url: url == null && nullToAbsent ? const Value.absent() : Value(url),
      width:
          width == null && nullToAbsent ? const Value.absent() : Value(width),
      checksum: checksum == null && nullToAbsent
          ? const Value.absent()
          : Value(checksum),
    );
  }

  factory GameTableData.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return GameTableData(
      id: serializer.fromJson<int>(json['id']),
      createdAt: serializer.fromJson<int>(json['createdAt']),
      firstRelease: serializer.fromJson<int>(json['firstRelease']),
      popularity: serializer.fromJson<double>(json['popularity']),
      summary: serializer.fromJson<String>(json['summary']),
      rating: serializer.fromJson<double>(json['rating']),
      name: serializer.fromJson<String>(json['name']),
      alphaChannel: serializer.fromJson<bool>(json['alphaChannel']),
      animated: serializer.fromJson<bool>(json['animated']),
      game: serializer.fromJson<int>(json['game']),
      height: serializer.fromJson<int>(json['height']),
      imageId: serializer.fromJson<String>(json['imageId']),
      url: serializer.fromJson<String>(json['url']),
      width: serializer.fromJson<int>(json['width']),
      checksum: serializer.fromJson<String>(json['checksum']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'createdAt': serializer.toJson<int>(createdAt),
      'firstRelease': serializer.toJson<int>(firstRelease),
      'popularity': serializer.toJson<double>(popularity),
      'summary': serializer.toJson<String>(summary),
      'rating': serializer.toJson<double>(rating),
      'name': serializer.toJson<String>(name),
      'alphaChannel': serializer.toJson<bool>(alphaChannel),
      'animated': serializer.toJson<bool>(animated),
      'game': serializer.toJson<int>(game),
      'height': serializer.toJson<int>(height),
      'imageId': serializer.toJson<String>(imageId),
      'url': serializer.toJson<String>(url),
      'width': serializer.toJson<int>(width),
      'checksum': serializer.toJson<String>(checksum),
    };
  }

  GameTableData copyWith(
          {int id,
          int createdAt,
          int firstRelease,
          double popularity,
          String summary,
          double rating,
          String name,
          bool alphaChannel,
          bool animated,
          int game,
          int height,
          String imageId,
          String url,
          int width,
          String checksum}) =>
      GameTableData(
        id: id ?? this.id,
        createdAt: createdAt ?? this.createdAt,
        firstRelease: firstRelease ?? this.firstRelease,
        popularity: popularity ?? this.popularity,
        summary: summary ?? this.summary,
        rating: rating ?? this.rating,
        name: name ?? this.name,
        alphaChannel: alphaChannel ?? this.alphaChannel,
        animated: animated ?? this.animated,
        game: game ?? this.game,
        height: height ?? this.height,
        imageId: imageId ?? this.imageId,
        url: url ?? this.url,
        width: width ?? this.width,
        checksum: checksum ?? this.checksum,
      );
  @override
  String toString() {
    return (StringBuffer('GameTableData(')
          ..write('id: $id, ')
          ..write('createdAt: $createdAt, ')
          ..write('firstRelease: $firstRelease, ')
          ..write('popularity: $popularity, ')
          ..write('summary: $summary, ')
          ..write('rating: $rating, ')
          ..write('name: $name, ')
          ..write('alphaChannel: $alphaChannel, ')
          ..write('animated: $animated, ')
          ..write('game: $game, ')
          ..write('height: $height, ')
          ..write('imageId: $imageId, ')
          ..write('url: $url, ')
          ..write('width: $width, ')
          ..write('checksum: $checksum')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          createdAt.hashCode,
          $mrjc(
              firstRelease.hashCode,
              $mrjc(
                  popularity.hashCode,
                  $mrjc(
                      summary.hashCode,
                      $mrjc(
                          rating.hashCode,
                          $mrjc(
                              name.hashCode,
                              $mrjc(
                                  alphaChannel.hashCode,
                                  $mrjc(
                                      animated.hashCode,
                                      $mrjc(
                                          game.hashCode,
                                          $mrjc(
                                              height.hashCode,
                                              $mrjc(
                                                  imageId.hashCode,
                                                  $mrjc(
                                                      url.hashCode,
                                                      $mrjc(
                                                          width.hashCode,
                                                          checksum
                                                              .hashCode)))))))))))))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is GameTableData &&
          other.id == this.id &&
          other.createdAt == this.createdAt &&
          other.firstRelease == this.firstRelease &&
          other.popularity == this.popularity &&
          other.summary == this.summary &&
          other.rating == this.rating &&
          other.name == this.name &&
          other.alphaChannel == this.alphaChannel &&
          other.animated == this.animated &&
          other.game == this.game &&
          other.height == this.height &&
          other.imageId == this.imageId &&
          other.url == this.url &&
          other.width == this.width &&
          other.checksum == this.checksum);
}

class GameTableCompanion extends UpdateCompanion<GameTableData> {
  final Value<int> id;
  final Value<int> createdAt;
  final Value<int> firstRelease;
  final Value<double> popularity;
  final Value<String> summary;
  final Value<double> rating;
  final Value<String> name;
  final Value<bool> alphaChannel;
  final Value<bool> animated;
  final Value<int> game;
  final Value<int> height;
  final Value<String> imageId;
  final Value<String> url;
  final Value<int> width;
  final Value<String> checksum;
  const GameTableCompanion({
    this.id = const Value.absent(),
    this.createdAt = const Value.absent(),
    this.firstRelease = const Value.absent(),
    this.popularity = const Value.absent(),
    this.summary = const Value.absent(),
    this.rating = const Value.absent(),
    this.name = const Value.absent(),
    this.alphaChannel = const Value.absent(),
    this.animated = const Value.absent(),
    this.game = const Value.absent(),
    this.height = const Value.absent(),
    this.imageId = const Value.absent(),
    this.url = const Value.absent(),
    this.width = const Value.absent(),
    this.checksum = const Value.absent(),
  });
  GameTableCompanion.insert({
    @required int id,
    @required int createdAt,
    @required int firstRelease,
    @required double popularity,
    @required String summary,
    @required double rating,
    @required String name,
    @required bool alphaChannel,
    @required bool animated,
    @required int game,
    @required int height,
    @required String imageId,
    @required String url,
    @required int width,
    @required String checksum,
  })  : id = Value(id),
        createdAt = Value(createdAt),
        firstRelease = Value(firstRelease),
        popularity = Value(popularity),
        summary = Value(summary),
        rating = Value(rating),
        name = Value(name),
        alphaChannel = Value(alphaChannel),
        animated = Value(animated),
        game = Value(game),
        height = Value(height),
        imageId = Value(imageId),
        url = Value(url),
        width = Value(width),
        checksum = Value(checksum);
  static Insertable<GameTableData> custom({
    Expression<int> id,
    Expression<int> createdAt,
    Expression<int> firstRelease,
    Expression<double> popularity,
    Expression<String> summary,
    Expression<double> rating,
    Expression<String> name,
    Expression<bool> alphaChannel,
    Expression<bool> animated,
    Expression<int> game,
    Expression<int> height,
    Expression<String> imageId,
    Expression<String> url,
    Expression<int> width,
    Expression<String> checksum,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (createdAt != null) 'created_at': createdAt,
      if (firstRelease != null) 'first_release': firstRelease,
      if (popularity != null) 'popularity': popularity,
      if (summary != null) 'summary': summary,
      if (rating != null) 'rating': rating,
      if (name != null) 'name': name,
      if (alphaChannel != null) 'alpha_channel': alphaChannel,
      if (animated != null) 'animated': animated,
      if (game != null) 'game': game,
      if (height != null) 'height': height,
      if (imageId != null) 'image_id': imageId,
      if (url != null) 'url': url,
      if (width != null) 'width': width,
      if (checksum != null) 'checksum': checksum,
    });
  }

  GameTableCompanion copyWith(
      {Value<int> id,
      Value<int> createdAt,
      Value<int> firstRelease,
      Value<double> popularity,
      Value<String> summary,
      Value<double> rating,
      Value<String> name,
      Value<bool> alphaChannel,
      Value<bool> animated,
      Value<int> game,
      Value<int> height,
      Value<String> imageId,
      Value<String> url,
      Value<int> width,
      Value<String> checksum}) {
    return GameTableCompanion(
      id: id ?? this.id,
      createdAt: createdAt ?? this.createdAt,
      firstRelease: firstRelease ?? this.firstRelease,
      popularity: popularity ?? this.popularity,
      summary: summary ?? this.summary,
      rating: rating ?? this.rating,
      name: name ?? this.name,
      alphaChannel: alphaChannel ?? this.alphaChannel,
      animated: animated ?? this.animated,
      game: game ?? this.game,
      height: height ?? this.height,
      imageId: imageId ?? this.imageId,
      url: url ?? this.url,
      width: width ?? this.width,
      checksum: checksum ?? this.checksum,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (createdAt.present) {
      map['created_at'] = Variable<int>(createdAt.value);
    }
    if (firstRelease.present) {
      map['first_release'] = Variable<int>(firstRelease.value);
    }
    if (popularity.present) {
      map['popularity'] = Variable<double>(popularity.value);
    }
    if (summary.present) {
      map['summary'] = Variable<String>(summary.value);
    }
    if (rating.present) {
      map['rating'] = Variable<double>(rating.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (alphaChannel.present) {
      map['alpha_channel'] = Variable<bool>(alphaChannel.value);
    }
    if (animated.present) {
      map['animated'] = Variable<bool>(animated.value);
    }
    if (game.present) {
      map['game'] = Variable<int>(game.value);
    }
    if (height.present) {
      map['height'] = Variable<int>(height.value);
    }
    if (imageId.present) {
      map['image_id'] = Variable<String>(imageId.value);
    }
    if (url.present) {
      map['url'] = Variable<String>(url.value);
    }
    if (width.present) {
      map['width'] = Variable<int>(width.value);
    }
    if (checksum.present) {
      map['checksum'] = Variable<String>(checksum.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('GameTableCompanion(')
          ..write('id: $id, ')
          ..write('createdAt: $createdAt, ')
          ..write('firstRelease: $firstRelease, ')
          ..write('popularity: $popularity, ')
          ..write('summary: $summary, ')
          ..write('rating: $rating, ')
          ..write('name: $name, ')
          ..write('alphaChannel: $alphaChannel, ')
          ..write('animated: $animated, ')
          ..write('game: $game, ')
          ..write('height: $height, ')
          ..write('imageId: $imageId, ')
          ..write('url: $url, ')
          ..write('width: $width, ')
          ..write('checksum: $checksum')
          ..write(')'))
        .toString();
  }
}

class $GameTableTable extends GameTable
    with TableInfo<$GameTableTable, GameTableData> {
  final GeneratedDatabase _db;
  final String _alias;
  $GameTableTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn(
      'id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _createdAtMeta = const VerificationMeta('createdAt');
  GeneratedIntColumn _createdAt;
  @override
  GeneratedIntColumn get createdAt => _createdAt ??= _constructCreatedAt();
  GeneratedIntColumn _constructCreatedAt() {
    return GeneratedIntColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  final VerificationMeta _firstReleaseMeta =
      const VerificationMeta('firstRelease');
  GeneratedIntColumn _firstRelease;
  @override
  GeneratedIntColumn get firstRelease =>
      _firstRelease ??= _constructFirstRelease();
  GeneratedIntColumn _constructFirstRelease() {
    return GeneratedIntColumn(
      'first_release',
      $tableName,
      false,
    );
  }

  final VerificationMeta _popularityMeta = const VerificationMeta('popularity');
  GeneratedRealColumn _popularity;
  @override
  GeneratedRealColumn get popularity => _popularity ??= _constructPopularity();
  GeneratedRealColumn _constructPopularity() {
    return GeneratedRealColumn(
      'popularity',
      $tableName,
      false,
    );
  }

  final VerificationMeta _summaryMeta = const VerificationMeta('summary');
  GeneratedTextColumn _summary;
  @override
  GeneratedTextColumn get summary => _summary ??= _constructSummary();
  GeneratedTextColumn _constructSummary() {
    return GeneratedTextColumn(
      'summary',
      $tableName,
      false,
    );
  }

  final VerificationMeta _ratingMeta = const VerificationMeta('rating');
  GeneratedRealColumn _rating;
  @override
  GeneratedRealColumn get rating => _rating ??= _constructRating();
  GeneratedRealColumn _constructRating() {
    return GeneratedRealColumn(
      'rating',
      $tableName,
      false,
    );
  }

  final VerificationMeta _nameMeta = const VerificationMeta('name');
  GeneratedTextColumn _name;
  @override
  GeneratedTextColumn get name => _name ??= _constructName();
  GeneratedTextColumn _constructName() {
    return GeneratedTextColumn(
      'name',
      $tableName,
      false,
    );
  }

  final VerificationMeta _alphaChannelMeta =
      const VerificationMeta('alphaChannel');
  GeneratedBoolColumn _alphaChannel;
  @override
  GeneratedBoolColumn get alphaChannel =>
      _alphaChannel ??= _constructAlphaChannel();
  GeneratedBoolColumn _constructAlphaChannel() {
    return GeneratedBoolColumn(
      'alpha_channel',
      $tableName,
      false,
    );
  }

  final VerificationMeta _animatedMeta = const VerificationMeta('animated');
  GeneratedBoolColumn _animated;
  @override
  GeneratedBoolColumn get animated => _animated ??= _constructAnimated();
  GeneratedBoolColumn _constructAnimated() {
    return GeneratedBoolColumn(
      'animated',
      $tableName,
      false,
    );
  }

  final VerificationMeta _gameMeta = const VerificationMeta('game');
  GeneratedIntColumn _game;
  @override
  GeneratedIntColumn get game => _game ??= _constructGame();
  GeneratedIntColumn _constructGame() {
    return GeneratedIntColumn(
      'game',
      $tableName,
      false,
    );
  }

  final VerificationMeta _heightMeta = const VerificationMeta('height');
  GeneratedIntColumn _height;
  @override
  GeneratedIntColumn get height => _height ??= _constructHeight();
  GeneratedIntColumn _constructHeight() {
    return GeneratedIntColumn(
      'height',
      $tableName,
      false,
    );
  }

  final VerificationMeta _imageIdMeta = const VerificationMeta('imageId');
  GeneratedTextColumn _imageId;
  @override
  GeneratedTextColumn get imageId => _imageId ??= _constructImageId();
  GeneratedTextColumn _constructImageId() {
    return GeneratedTextColumn(
      'image_id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _urlMeta = const VerificationMeta('url');
  GeneratedTextColumn _url;
  @override
  GeneratedTextColumn get url => _url ??= _constructUrl();
  GeneratedTextColumn _constructUrl() {
    return GeneratedTextColumn(
      'url',
      $tableName,
      false,
    );
  }

  final VerificationMeta _widthMeta = const VerificationMeta('width');
  GeneratedIntColumn _width;
  @override
  GeneratedIntColumn get width => _width ??= _constructWidth();
  GeneratedIntColumn _constructWidth() {
    return GeneratedIntColumn(
      'width',
      $tableName,
      false,
    );
  }

  final VerificationMeta _checksumMeta = const VerificationMeta('checksum');
  GeneratedTextColumn _checksum;
  @override
  GeneratedTextColumn get checksum => _checksum ??= _constructChecksum();
  GeneratedTextColumn _constructChecksum() {
    return GeneratedTextColumn(
      'checksum',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [
        id,
        createdAt,
        firstRelease,
        popularity,
        summary,
        rating,
        name,
        alphaChannel,
        animated,
        game,
        height,
        imageId,
        url,
        width,
        checksum
      ];
  @override
  $GameTableTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'game_table';
  @override
  final String actualTableName = 'game_table';
  @override
  VerificationContext validateIntegrity(Insertable<GameTableData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    } else if (isInserting) {
      context.missing(_idMeta);
    }
    if (data.containsKey('created_at')) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableOrUnknown(data['created_at'], _createdAtMeta));
    } else if (isInserting) {
      context.missing(_createdAtMeta);
    }
    if (data.containsKey('first_release')) {
      context.handle(
          _firstReleaseMeta,
          firstRelease.isAcceptableOrUnknown(
              data['first_release'], _firstReleaseMeta));
    } else if (isInserting) {
      context.missing(_firstReleaseMeta);
    }
    if (data.containsKey('popularity')) {
      context.handle(
          _popularityMeta,
          popularity.isAcceptableOrUnknown(
              data['popularity'], _popularityMeta));
    } else if (isInserting) {
      context.missing(_popularityMeta);
    }
    if (data.containsKey('summary')) {
      context.handle(_summaryMeta,
          summary.isAcceptableOrUnknown(data['summary'], _summaryMeta));
    } else if (isInserting) {
      context.missing(_summaryMeta);
    }
    if (data.containsKey('rating')) {
      context.handle(_ratingMeta,
          rating.isAcceptableOrUnknown(data['rating'], _ratingMeta));
    } else if (isInserting) {
      context.missing(_ratingMeta);
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name'], _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (data.containsKey('alpha_channel')) {
      context.handle(
          _alphaChannelMeta,
          alphaChannel.isAcceptableOrUnknown(
              data['alpha_channel'], _alphaChannelMeta));
    } else if (isInserting) {
      context.missing(_alphaChannelMeta);
    }
    if (data.containsKey('animated')) {
      context.handle(_animatedMeta,
          animated.isAcceptableOrUnknown(data['animated'], _animatedMeta));
    } else if (isInserting) {
      context.missing(_animatedMeta);
    }
    if (data.containsKey('game')) {
      context.handle(
          _gameMeta, game.isAcceptableOrUnknown(data['game'], _gameMeta));
    } else if (isInserting) {
      context.missing(_gameMeta);
    }
    if (data.containsKey('height')) {
      context.handle(_heightMeta,
          height.isAcceptableOrUnknown(data['height'], _heightMeta));
    } else if (isInserting) {
      context.missing(_heightMeta);
    }
    if (data.containsKey('image_id')) {
      context.handle(_imageIdMeta,
          imageId.isAcceptableOrUnknown(data['image_id'], _imageIdMeta));
    } else if (isInserting) {
      context.missing(_imageIdMeta);
    }
    if (data.containsKey('url')) {
      context.handle(
          _urlMeta, url.isAcceptableOrUnknown(data['url'], _urlMeta));
    } else if (isInserting) {
      context.missing(_urlMeta);
    }
    if (data.containsKey('width')) {
      context.handle(
          _widthMeta, width.isAcceptableOrUnknown(data['width'], _widthMeta));
    } else if (isInserting) {
      context.missing(_widthMeta);
    }
    if (data.containsKey('checksum')) {
      context.handle(_checksumMeta,
          checksum.isAcceptableOrUnknown(data['checksum'], _checksumMeta));
    } else if (isInserting) {
      context.missing(_checksumMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {name};
  @override
  GameTableData map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return GameTableData.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $GameTableTable createAlias(String alias) {
    return $GameTableTable(_db, alias);
  }
}

class PlayerPerspectivesTableData extends DataClass
    implements Insertable<PlayerPerspectivesTableData> {
  final String fkName;
  final int id;
  final int createdAt;
  final String name;
  final String slug;
  final int updatedAt;
  final String url;
  final String checksum;
  PlayerPerspectivesTableData(
      {@required this.fkName,
      @required this.id,
      @required this.createdAt,
      @required this.name,
      @required this.slug,
      @required this.updatedAt,
      @required this.url,
      @required this.checksum});
  factory PlayerPerspectivesTableData.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final stringType = db.typeSystem.forDartType<String>();
    final intType = db.typeSystem.forDartType<int>();
    return PlayerPerspectivesTableData(
      fkName:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}fk_name']),
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      createdAt:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
      name: stringType.mapFromDatabaseResponse(data['${effectivePrefix}name']),
      slug: stringType.mapFromDatabaseResponse(data['${effectivePrefix}slug']),
      updatedAt:
          intType.mapFromDatabaseResponse(data['${effectivePrefix}updated_at']),
      url: stringType.mapFromDatabaseResponse(data['${effectivePrefix}url']),
      checksum: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}checksum']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || fkName != null) {
      map['fk_name'] = Variable<String>(fkName);
    }
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || createdAt != null) {
      map['created_at'] = Variable<int>(createdAt);
    }
    if (!nullToAbsent || name != null) {
      map['name'] = Variable<String>(name);
    }
    if (!nullToAbsent || slug != null) {
      map['slug'] = Variable<String>(slug);
    }
    if (!nullToAbsent || updatedAt != null) {
      map['updated_at'] = Variable<int>(updatedAt);
    }
    if (!nullToAbsent || url != null) {
      map['url'] = Variable<String>(url);
    }
    if (!nullToAbsent || checksum != null) {
      map['checksum'] = Variable<String>(checksum);
    }
    return map;
  }

  PlayerPerspectivesTableCompanion toCompanion(bool nullToAbsent) {
    return PlayerPerspectivesTableCompanion(
      fkName:
          fkName == null && nullToAbsent ? const Value.absent() : Value(fkName),
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      createdAt: createdAt == null && nullToAbsent
          ? const Value.absent()
          : Value(createdAt),
      name: name == null && nullToAbsent ? const Value.absent() : Value(name),
      slug: slug == null && nullToAbsent ? const Value.absent() : Value(slug),
      updatedAt: updatedAt == null && nullToAbsent
          ? const Value.absent()
          : Value(updatedAt),
      url: url == null && nullToAbsent ? const Value.absent() : Value(url),
      checksum: checksum == null && nullToAbsent
          ? const Value.absent()
          : Value(checksum),
    );
  }

  factory PlayerPerspectivesTableData.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return PlayerPerspectivesTableData(
      fkName: serializer.fromJson<String>(json['fkName']),
      id: serializer.fromJson<int>(json['id']),
      createdAt: serializer.fromJson<int>(json['createdAt']),
      name: serializer.fromJson<String>(json['name']),
      slug: serializer.fromJson<String>(json['slug']),
      updatedAt: serializer.fromJson<int>(json['updatedAt']),
      url: serializer.fromJson<String>(json['url']),
      checksum: serializer.fromJson<String>(json['checksum']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'fkName': serializer.toJson<String>(fkName),
      'id': serializer.toJson<int>(id),
      'createdAt': serializer.toJson<int>(createdAt),
      'name': serializer.toJson<String>(name),
      'slug': serializer.toJson<String>(slug),
      'updatedAt': serializer.toJson<int>(updatedAt),
      'url': serializer.toJson<String>(url),
      'checksum': serializer.toJson<String>(checksum),
    };
  }

  PlayerPerspectivesTableData copyWith(
          {String fkName,
          int id,
          int createdAt,
          String name,
          String slug,
          int updatedAt,
          String url,
          String checksum}) =>
      PlayerPerspectivesTableData(
        fkName: fkName ?? this.fkName,
        id: id ?? this.id,
        createdAt: createdAt ?? this.createdAt,
        name: name ?? this.name,
        slug: slug ?? this.slug,
        updatedAt: updatedAt ?? this.updatedAt,
        url: url ?? this.url,
        checksum: checksum ?? this.checksum,
      );
  @override
  String toString() {
    return (StringBuffer('PlayerPerspectivesTableData(')
          ..write('fkName: $fkName, ')
          ..write('id: $id, ')
          ..write('createdAt: $createdAt, ')
          ..write('name: $name, ')
          ..write('slug: $slug, ')
          ..write('updatedAt: $updatedAt, ')
          ..write('url: $url, ')
          ..write('checksum: $checksum')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      fkName.hashCode,
      $mrjc(
          id.hashCode,
          $mrjc(
              createdAt.hashCode,
              $mrjc(
                  name.hashCode,
                  $mrjc(
                      slug.hashCode,
                      $mrjc(updatedAt.hashCode,
                          $mrjc(url.hashCode, checksum.hashCode))))))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is PlayerPerspectivesTableData &&
          other.fkName == this.fkName &&
          other.id == this.id &&
          other.createdAt == this.createdAt &&
          other.name == this.name &&
          other.slug == this.slug &&
          other.updatedAt == this.updatedAt &&
          other.url == this.url &&
          other.checksum == this.checksum);
}

class PlayerPerspectivesTableCompanion
    extends UpdateCompanion<PlayerPerspectivesTableData> {
  final Value<String> fkName;
  final Value<int> id;
  final Value<int> createdAt;
  final Value<String> name;
  final Value<String> slug;
  final Value<int> updatedAt;
  final Value<String> url;
  final Value<String> checksum;
  const PlayerPerspectivesTableCompanion({
    this.fkName = const Value.absent(),
    this.id = const Value.absent(),
    this.createdAt = const Value.absent(),
    this.name = const Value.absent(),
    this.slug = const Value.absent(),
    this.updatedAt = const Value.absent(),
    this.url = const Value.absent(),
    this.checksum = const Value.absent(),
  });
  PlayerPerspectivesTableCompanion.insert({
    @required String fkName,
    @required int id,
    @required int createdAt,
    @required String name,
    @required String slug,
    @required int updatedAt,
    @required String url,
    @required String checksum,
  })  : fkName = Value(fkName),
        id = Value(id),
        createdAt = Value(createdAt),
        name = Value(name),
        slug = Value(slug),
        updatedAt = Value(updatedAt),
        url = Value(url),
        checksum = Value(checksum);
  static Insertable<PlayerPerspectivesTableData> custom({
    Expression<String> fkName,
    Expression<int> id,
    Expression<int> createdAt,
    Expression<String> name,
    Expression<String> slug,
    Expression<int> updatedAt,
    Expression<String> url,
    Expression<String> checksum,
  }) {
    return RawValuesInsertable({
      if (fkName != null) 'fk_name': fkName,
      if (id != null) 'id': id,
      if (createdAt != null) 'created_at': createdAt,
      if (name != null) 'name': name,
      if (slug != null) 'slug': slug,
      if (updatedAt != null) 'updated_at': updatedAt,
      if (url != null) 'url': url,
      if (checksum != null) 'checksum': checksum,
    });
  }

  PlayerPerspectivesTableCompanion copyWith(
      {Value<String> fkName,
      Value<int> id,
      Value<int> createdAt,
      Value<String> name,
      Value<String> slug,
      Value<int> updatedAt,
      Value<String> url,
      Value<String> checksum}) {
    return PlayerPerspectivesTableCompanion(
      fkName: fkName ?? this.fkName,
      id: id ?? this.id,
      createdAt: createdAt ?? this.createdAt,
      name: name ?? this.name,
      slug: slug ?? this.slug,
      updatedAt: updatedAt ?? this.updatedAt,
      url: url ?? this.url,
      checksum: checksum ?? this.checksum,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (fkName.present) {
      map['fk_name'] = Variable<String>(fkName.value);
    }
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (createdAt.present) {
      map['created_at'] = Variable<int>(createdAt.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (slug.present) {
      map['slug'] = Variable<String>(slug.value);
    }
    if (updatedAt.present) {
      map['updated_at'] = Variable<int>(updatedAt.value);
    }
    if (url.present) {
      map['url'] = Variable<String>(url.value);
    }
    if (checksum.present) {
      map['checksum'] = Variable<String>(checksum.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('PlayerPerspectivesTableCompanion(')
          ..write('fkName: $fkName, ')
          ..write('id: $id, ')
          ..write('createdAt: $createdAt, ')
          ..write('name: $name, ')
          ..write('slug: $slug, ')
          ..write('updatedAt: $updatedAt, ')
          ..write('url: $url, ')
          ..write('checksum: $checksum')
          ..write(')'))
        .toString();
  }
}

class $PlayerPerspectivesTableTable extends PlayerPerspectivesTable
    with TableInfo<$PlayerPerspectivesTableTable, PlayerPerspectivesTableData> {
  final GeneratedDatabase _db;
  final String _alias;
  $PlayerPerspectivesTableTable(this._db, [this._alias]);
  final VerificationMeta _fkNameMeta = const VerificationMeta('fkName');
  GeneratedTextColumn _fkName;
  @override
  GeneratedTextColumn get fkName => _fkName ??= _constructFkName();
  GeneratedTextColumn _constructFkName() {
    return GeneratedTextColumn(
      'fk_name',
      $tableName,
      false,
    );
  }

  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn(
      'id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _createdAtMeta = const VerificationMeta('createdAt');
  GeneratedIntColumn _createdAt;
  @override
  GeneratedIntColumn get createdAt => _createdAt ??= _constructCreatedAt();
  GeneratedIntColumn _constructCreatedAt() {
    return GeneratedIntColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  final VerificationMeta _nameMeta = const VerificationMeta('name');
  GeneratedTextColumn _name;
  @override
  GeneratedTextColumn get name => _name ??= _constructName();
  GeneratedTextColumn _constructName() {
    return GeneratedTextColumn(
      'name',
      $tableName,
      false,
    );
  }

  final VerificationMeta _slugMeta = const VerificationMeta('slug');
  GeneratedTextColumn _slug;
  @override
  GeneratedTextColumn get slug => _slug ??= _constructSlug();
  GeneratedTextColumn _constructSlug() {
    return GeneratedTextColumn(
      'slug',
      $tableName,
      false,
    );
  }

  final VerificationMeta _updatedAtMeta = const VerificationMeta('updatedAt');
  GeneratedIntColumn _updatedAt;
  @override
  GeneratedIntColumn get updatedAt => _updatedAt ??= _constructUpdatedAt();
  GeneratedIntColumn _constructUpdatedAt() {
    return GeneratedIntColumn(
      'updated_at',
      $tableName,
      false,
    );
  }

  final VerificationMeta _urlMeta = const VerificationMeta('url');
  GeneratedTextColumn _url;
  @override
  GeneratedTextColumn get url => _url ??= _constructUrl();
  GeneratedTextColumn _constructUrl() {
    return GeneratedTextColumn(
      'url',
      $tableName,
      false,
    );
  }

  final VerificationMeta _checksumMeta = const VerificationMeta('checksum');
  GeneratedTextColumn _checksum;
  @override
  GeneratedTextColumn get checksum => _checksum ??= _constructChecksum();
  GeneratedTextColumn _constructChecksum() {
    return GeneratedTextColumn(
      'checksum',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns =>
      [fkName, id, createdAt, name, slug, updatedAt, url, checksum];
  @override
  $PlayerPerspectivesTableTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'player_perspectives_table';
  @override
  final String actualTableName = 'player_perspectives_table';
  @override
  VerificationContext validateIntegrity(
      Insertable<PlayerPerspectivesTableData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('fk_name')) {
      context.handle(_fkNameMeta,
          fkName.isAcceptableOrUnknown(data['fk_name'], _fkNameMeta));
    } else if (isInserting) {
      context.missing(_fkNameMeta);
    }
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    } else if (isInserting) {
      context.missing(_idMeta);
    }
    if (data.containsKey('created_at')) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableOrUnknown(data['created_at'], _createdAtMeta));
    } else if (isInserting) {
      context.missing(_createdAtMeta);
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name'], _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (data.containsKey('slug')) {
      context.handle(
          _slugMeta, slug.isAcceptableOrUnknown(data['slug'], _slugMeta));
    } else if (isInserting) {
      context.missing(_slugMeta);
    }
    if (data.containsKey('updated_at')) {
      context.handle(_updatedAtMeta,
          updatedAt.isAcceptableOrUnknown(data['updated_at'], _updatedAtMeta));
    } else if (isInserting) {
      context.missing(_updatedAtMeta);
    }
    if (data.containsKey('url')) {
      context.handle(
          _urlMeta, url.isAcceptableOrUnknown(data['url'], _urlMeta));
    } else if (isInserting) {
      context.missing(_urlMeta);
    }
    if (data.containsKey('checksum')) {
      context.handle(_checksumMeta,
          checksum.isAcceptableOrUnknown(data['checksum'], _checksumMeta));
    } else if (isInserting) {
      context.missing(_checksumMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => <GeneratedColumn>{};
  @override
  PlayerPerspectivesTableData map(Map<String, dynamic> data,
      {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return PlayerPerspectivesTableData.fromData(data, _db,
        prefix: effectivePrefix);
  }

  @override
  $PlayerPerspectivesTableTable createAlias(String alias) {
    return $PlayerPerspectivesTableTable(_db, alias);
  }
}

class ScreenshotsTableData extends DataClass
    implements Insertable<ScreenshotsTableData> {
  final String fkName;
  final int id;
  final int game;
  final int height;
  final String imageId;
  final String url;
  final int width;
  final String checksum;
  ScreenshotsTableData(
      {@required this.fkName,
      @required this.id,
      @required this.game,
      @required this.height,
      @required this.imageId,
      @required this.url,
      @required this.width,
      @required this.checksum});
  factory ScreenshotsTableData.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final stringType = db.typeSystem.forDartType<String>();
    final intType = db.typeSystem.forDartType<int>();
    return ScreenshotsTableData(
      fkName:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}fk_name']),
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      game: intType.mapFromDatabaseResponse(data['${effectivePrefix}game']),
      height: intType.mapFromDatabaseResponse(data['${effectivePrefix}height']),
      imageId: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}image_id']),
      url: stringType.mapFromDatabaseResponse(data['${effectivePrefix}url']),
      width: intType.mapFromDatabaseResponse(data['${effectivePrefix}width']),
      checksum: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}checksum']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || fkName != null) {
      map['fk_name'] = Variable<String>(fkName);
    }
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || game != null) {
      map['game'] = Variable<int>(game);
    }
    if (!nullToAbsent || height != null) {
      map['height'] = Variable<int>(height);
    }
    if (!nullToAbsent || imageId != null) {
      map['image_id'] = Variable<String>(imageId);
    }
    if (!nullToAbsent || url != null) {
      map['url'] = Variable<String>(url);
    }
    if (!nullToAbsent || width != null) {
      map['width'] = Variable<int>(width);
    }
    if (!nullToAbsent || checksum != null) {
      map['checksum'] = Variable<String>(checksum);
    }
    return map;
  }

  ScreenshotsTableCompanion toCompanion(bool nullToAbsent) {
    return ScreenshotsTableCompanion(
      fkName:
          fkName == null && nullToAbsent ? const Value.absent() : Value(fkName),
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      game: game == null && nullToAbsent ? const Value.absent() : Value(game),
      height:
          height == null && nullToAbsent ? const Value.absent() : Value(height),
      imageId: imageId == null && nullToAbsent
          ? const Value.absent()
          : Value(imageId),
      url: url == null && nullToAbsent ? const Value.absent() : Value(url),
      width:
          width == null && nullToAbsent ? const Value.absent() : Value(width),
      checksum: checksum == null && nullToAbsent
          ? const Value.absent()
          : Value(checksum),
    );
  }

  factory ScreenshotsTableData.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return ScreenshotsTableData(
      fkName: serializer.fromJson<String>(json['fkName']),
      id: serializer.fromJson<int>(json['id']),
      game: serializer.fromJson<int>(json['game']),
      height: serializer.fromJson<int>(json['height']),
      imageId: serializer.fromJson<String>(json['imageId']),
      url: serializer.fromJson<String>(json['url']),
      width: serializer.fromJson<int>(json['width']),
      checksum: serializer.fromJson<String>(json['checksum']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'fkName': serializer.toJson<String>(fkName),
      'id': serializer.toJson<int>(id),
      'game': serializer.toJson<int>(game),
      'height': serializer.toJson<int>(height),
      'imageId': serializer.toJson<String>(imageId),
      'url': serializer.toJson<String>(url),
      'width': serializer.toJson<int>(width),
      'checksum': serializer.toJson<String>(checksum),
    };
  }

  ScreenshotsTableData copyWith(
          {String fkName,
          int id,
          int game,
          int height,
          String imageId,
          String url,
          int width,
          String checksum}) =>
      ScreenshotsTableData(
        fkName: fkName ?? this.fkName,
        id: id ?? this.id,
        game: game ?? this.game,
        height: height ?? this.height,
        imageId: imageId ?? this.imageId,
        url: url ?? this.url,
        width: width ?? this.width,
        checksum: checksum ?? this.checksum,
      );
  @override
  String toString() {
    return (StringBuffer('ScreenshotsTableData(')
          ..write('fkName: $fkName, ')
          ..write('id: $id, ')
          ..write('game: $game, ')
          ..write('height: $height, ')
          ..write('imageId: $imageId, ')
          ..write('url: $url, ')
          ..write('width: $width, ')
          ..write('checksum: $checksum')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      fkName.hashCode,
      $mrjc(
          id.hashCode,
          $mrjc(
              game.hashCode,
              $mrjc(
                  height.hashCode,
                  $mrjc(
                      imageId.hashCode,
                      $mrjc(url.hashCode,
                          $mrjc(width.hashCode, checksum.hashCode))))))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is ScreenshotsTableData &&
          other.fkName == this.fkName &&
          other.id == this.id &&
          other.game == this.game &&
          other.height == this.height &&
          other.imageId == this.imageId &&
          other.url == this.url &&
          other.width == this.width &&
          other.checksum == this.checksum);
}

class ScreenshotsTableCompanion extends UpdateCompanion<ScreenshotsTableData> {
  final Value<String> fkName;
  final Value<int> id;
  final Value<int> game;
  final Value<int> height;
  final Value<String> imageId;
  final Value<String> url;
  final Value<int> width;
  final Value<String> checksum;
  const ScreenshotsTableCompanion({
    this.fkName = const Value.absent(),
    this.id = const Value.absent(),
    this.game = const Value.absent(),
    this.height = const Value.absent(),
    this.imageId = const Value.absent(),
    this.url = const Value.absent(),
    this.width = const Value.absent(),
    this.checksum = const Value.absent(),
  });
  ScreenshotsTableCompanion.insert({
    @required String fkName,
    @required int id,
    @required int game,
    @required int height,
    @required String imageId,
    @required String url,
    @required int width,
    @required String checksum,
  })  : fkName = Value(fkName),
        id = Value(id),
        game = Value(game),
        height = Value(height),
        imageId = Value(imageId),
        url = Value(url),
        width = Value(width),
        checksum = Value(checksum);
  static Insertable<ScreenshotsTableData> custom({
    Expression<String> fkName,
    Expression<int> id,
    Expression<int> game,
    Expression<int> height,
    Expression<String> imageId,
    Expression<String> url,
    Expression<int> width,
    Expression<String> checksum,
  }) {
    return RawValuesInsertable({
      if (fkName != null) 'fk_name': fkName,
      if (id != null) 'id': id,
      if (game != null) 'game': game,
      if (height != null) 'height': height,
      if (imageId != null) 'image_id': imageId,
      if (url != null) 'url': url,
      if (width != null) 'width': width,
      if (checksum != null) 'checksum': checksum,
    });
  }

  ScreenshotsTableCompanion copyWith(
      {Value<String> fkName,
      Value<int> id,
      Value<int> game,
      Value<int> height,
      Value<String> imageId,
      Value<String> url,
      Value<int> width,
      Value<String> checksum}) {
    return ScreenshotsTableCompanion(
      fkName: fkName ?? this.fkName,
      id: id ?? this.id,
      game: game ?? this.game,
      height: height ?? this.height,
      imageId: imageId ?? this.imageId,
      url: url ?? this.url,
      width: width ?? this.width,
      checksum: checksum ?? this.checksum,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (fkName.present) {
      map['fk_name'] = Variable<String>(fkName.value);
    }
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (game.present) {
      map['game'] = Variable<int>(game.value);
    }
    if (height.present) {
      map['height'] = Variable<int>(height.value);
    }
    if (imageId.present) {
      map['image_id'] = Variable<String>(imageId.value);
    }
    if (url.present) {
      map['url'] = Variable<String>(url.value);
    }
    if (width.present) {
      map['width'] = Variable<int>(width.value);
    }
    if (checksum.present) {
      map['checksum'] = Variable<String>(checksum.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('ScreenshotsTableCompanion(')
          ..write('fkName: $fkName, ')
          ..write('id: $id, ')
          ..write('game: $game, ')
          ..write('height: $height, ')
          ..write('imageId: $imageId, ')
          ..write('url: $url, ')
          ..write('width: $width, ')
          ..write('checksum: $checksum')
          ..write(')'))
        .toString();
  }
}

class $ScreenshotsTableTable extends ScreenshotsTable
    with TableInfo<$ScreenshotsTableTable, ScreenshotsTableData> {
  final GeneratedDatabase _db;
  final String _alias;
  $ScreenshotsTableTable(this._db, [this._alias]);
  final VerificationMeta _fkNameMeta = const VerificationMeta('fkName');
  GeneratedTextColumn _fkName;
  @override
  GeneratedTextColumn get fkName => _fkName ??= _constructFkName();
  GeneratedTextColumn _constructFkName() {
    return GeneratedTextColumn(
      'fk_name',
      $tableName,
      false,
    );
  }

  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn(
      'id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _gameMeta = const VerificationMeta('game');
  GeneratedIntColumn _game;
  @override
  GeneratedIntColumn get game => _game ??= _constructGame();
  GeneratedIntColumn _constructGame() {
    return GeneratedIntColumn(
      'game',
      $tableName,
      false,
    );
  }

  final VerificationMeta _heightMeta = const VerificationMeta('height');
  GeneratedIntColumn _height;
  @override
  GeneratedIntColumn get height => _height ??= _constructHeight();
  GeneratedIntColumn _constructHeight() {
    return GeneratedIntColumn(
      'height',
      $tableName,
      false,
    );
  }

  final VerificationMeta _imageIdMeta = const VerificationMeta('imageId');
  GeneratedTextColumn _imageId;
  @override
  GeneratedTextColumn get imageId => _imageId ??= _constructImageId();
  GeneratedTextColumn _constructImageId() {
    return GeneratedTextColumn(
      'image_id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _urlMeta = const VerificationMeta('url');
  GeneratedTextColumn _url;
  @override
  GeneratedTextColumn get url => _url ??= _constructUrl();
  GeneratedTextColumn _constructUrl() {
    return GeneratedTextColumn(
      'url',
      $tableName,
      false,
    );
  }

  final VerificationMeta _widthMeta = const VerificationMeta('width');
  GeneratedIntColumn _width;
  @override
  GeneratedIntColumn get width => _width ??= _constructWidth();
  GeneratedIntColumn _constructWidth() {
    return GeneratedIntColumn(
      'width',
      $tableName,
      false,
    );
  }

  final VerificationMeta _checksumMeta = const VerificationMeta('checksum');
  GeneratedTextColumn _checksum;
  @override
  GeneratedTextColumn get checksum => _checksum ??= _constructChecksum();
  GeneratedTextColumn _constructChecksum() {
    return GeneratedTextColumn(
      'checksum',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns =>
      [fkName, id, game, height, imageId, url, width, checksum];
  @override
  $ScreenshotsTableTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'screenshots_table';
  @override
  final String actualTableName = 'screenshots_table';
  @override
  VerificationContext validateIntegrity(
      Insertable<ScreenshotsTableData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('fk_name')) {
      context.handle(_fkNameMeta,
          fkName.isAcceptableOrUnknown(data['fk_name'], _fkNameMeta));
    } else if (isInserting) {
      context.missing(_fkNameMeta);
    }
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    } else if (isInserting) {
      context.missing(_idMeta);
    }
    if (data.containsKey('game')) {
      context.handle(
          _gameMeta, game.isAcceptableOrUnknown(data['game'], _gameMeta));
    } else if (isInserting) {
      context.missing(_gameMeta);
    }
    if (data.containsKey('height')) {
      context.handle(_heightMeta,
          height.isAcceptableOrUnknown(data['height'], _heightMeta));
    } else if (isInserting) {
      context.missing(_heightMeta);
    }
    if (data.containsKey('image_id')) {
      context.handle(_imageIdMeta,
          imageId.isAcceptableOrUnknown(data['image_id'], _imageIdMeta));
    } else if (isInserting) {
      context.missing(_imageIdMeta);
    }
    if (data.containsKey('url')) {
      context.handle(
          _urlMeta, url.isAcceptableOrUnknown(data['url'], _urlMeta));
    } else if (isInserting) {
      context.missing(_urlMeta);
    }
    if (data.containsKey('width')) {
      context.handle(
          _widthMeta, width.isAcceptableOrUnknown(data['width'], _widthMeta));
    } else if (isInserting) {
      context.missing(_widthMeta);
    }
    if (data.containsKey('checksum')) {
      context.handle(_checksumMeta,
          checksum.isAcceptableOrUnknown(data['checksum'], _checksumMeta));
    } else if (isInserting) {
      context.missing(_checksumMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => <GeneratedColumn>{};
  @override
  ScreenshotsTableData map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return ScreenshotsTableData.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $ScreenshotsTableTable createAlias(String alias) {
    return $ScreenshotsTableTable(_db, alias);
  }
}

abstract class _$AppDatabase extends GeneratedDatabase {
  _$AppDatabase(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  $GameTableTable _gameTable;
  $GameTableTable get gameTable => _gameTable ??= $GameTableTable(this);
  $PlayerPerspectivesTableTable _playerPerspectivesTable;
  $PlayerPerspectivesTableTable get playerPerspectivesTable =>
      _playerPerspectivesTable ??= $PlayerPerspectivesTableTable(this);
  $ScreenshotsTableTable _screenshotsTable;
  $ScreenshotsTableTable get screenshotsTable =>
      _screenshotsTable ??= $ScreenshotsTableTable(this);
  GameDao _gameDao;
  GameDao get gameDao => _gameDao ??= GameDao(this as AppDatabase);
  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities =>
      [gameTable, playerPerspectivesTable, screenshotsTable];
}
