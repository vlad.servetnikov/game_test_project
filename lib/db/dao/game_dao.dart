import 'package:game_project/db/app_database.dart';
import 'package:game_project/db/tables/game_table.dart';
import 'package:game_project/db/tables/player_perspectives_table.dart';
import 'package:game_project/db/tables/screenshots_table.dart';
import 'package:game_project/models/game_model.dart';
import 'package:game_project/models/player_perspectives_model.dart';
import 'package:game_project/models/screenshots_model.dart';
import 'package:moor_flutter/moor_flutter.dart';

part 'game_dao.g.dart';

// flutter packages pub run build_runner build --delete-conflicting-outputs

@UseDao(tables: [GameTable, PlayerPerspectivesTable, ScreenshotsTable])
class GameDao extends DatabaseAccessor<AppDatabase> with _$GameDaoMixin {
  final AppDatabase db;

  GameDao(this.db) : super(db);

  Future<void> insertGame(GameModel gameModel) async {
    await into(gameTable).insert(
        GameTableCompanion(
            id: Value(gameModel.id ?? 0),
            createdAt: Value(gameModel.createdAt ?? 0),
            firstRelease: Value(gameModel.firstRelease ?? 0),
            popularity: Value(gameModel.popularity ?? 0.0),
            summary: Value(gameModel.summary ?? ''),
            rating: Value(gameModel.rating ?? 0.0),
            name: Value(gameModel.name ?? ''),
            alphaChannel: Value(gameModel?.cover?.alphaChannel ?? false),
            animated: Value(gameModel?.cover?.animated ?? false),
            game: Value(gameModel?.cover?.game ?? 0),
            height: Value(gameModel?.cover?.height ?? 0),
            imageId: Value(gameModel?.cover?.imageId ?? ''),
            url: Value(gameModel?.cover?.url ?? ''),
            width: Value(gameModel?.cover?.width ?? 0),
            checksum: Value(gameModel?.cover?.checksum ?? '')),
        mode: InsertMode.insertOrReplace);


    List<PlayerPerspectivesTableCompanion> perspectivesList = [];

    if (gameModel.perspectives != null) {
      for (PlayerPerspectivesModel perspectivesModel
          in gameModel.perspectives) {
        perspectivesList.add(PlayerPerspectivesTableCompanion(
            fkName: Value(gameModel.name),
            id: Value(perspectivesModel.id ?? 0),
            createdAt: Value(perspectivesModel.createdAt ?? 0),
            name: Value(perspectivesModel.name ?? ''),
            slug: Value(perspectivesModel.slug ?? ''),
            updatedAt: Value(perspectivesModel.updatedAt ?? 0),
            url: Value(perspectivesModel.url ?? ''),
            checksum: Value(perspectivesModel.checksum ?? '')));
      }
    }
    List<ScreenshotsTableCompanion> screenshotsList = [];

    if (gameModel.screenshots != null) {
      for (ScreenshotsModel screenshotsModel in gameModel.screenshots) {
        screenshotsList.add(ScreenshotsTableCompanion(
            fkName: Value(gameModel.name),
            id: Value(screenshotsModel.id ?? 0),
            game: Value(screenshotsModel.game ?? ''),
            height: Value(screenshotsModel.height ?? 0),
            imageId: Value(screenshotsModel.imageId ?? 0),
            url: Value(screenshotsModel.url ?? ''),
            width: Value(screenshotsModel.width ?? 0),
            checksum: Value(screenshotsModel.checksum ?? '')));
      }
    }

    await batch((batch) {
      if (perspectivesList.isNotEmpty)
        batch.insertAll(playerPerspectivesTable, perspectivesList,
            mode: InsertMode.insertOrReplace);
      if (screenshotsList.isNotEmpty)
        batch.insertAll(screenshotsTable, screenshotsList,
            mode: InsertMode.insertOrReplace);
    });
  }

  Future<List<GameModel>> fetchGames() async {
    var query = select(gameTable);

    query.orderBy([
      (game) => OrderingTerm(expression: game.name, mode: OrderingMode.asc)
    ]);

    List<TypedResult> res = await query.join([
      leftOuterJoin(playerPerspectivesTable,
          gameTable.name.equalsExp(playerPerspectivesTable.fkName)),
      leftOuterJoin(
          screenshotsTable, gameTable.name.equalsExp(screenshotsTable.fkName)),
    ]).get();

    return _parseJoinedGames(res);
  }

  List<GameModel> _parseJoinedGames(List<TypedResult> res) {
    Map<String, GameModel> games = {};
    GameTableData gameTableData;
    PlayerPerspectivesTableData perspectivesTableData;
    ScreenshotsTableData screenshotsTableData;
    GameModel gameModel;
    PlayerPerspectivesModel perspectivesModel;
    ScreenshotsModel screenshotsModel;

    for (TypedResult row in res) {
      gameTableData = row.readTableOrNull(gameTable);
      if (gameTableData == null) continue;
      gameModel = GameModel.fromJson(gameTableData.toJson());

      if (gameModel.perspectives == null) gameModel.perspectives = [];
      games.putIfAbsent(gameTableData.name, () => gameModel);

      perspectivesTableData = row.readTableOrNull(playerPerspectivesTable);
      if (perspectivesTableData != null) {
        perspectivesModel =
            PlayerPerspectivesModel.fromJson(perspectivesTableData.toJson());
        games[perspectivesTableData.fkName].perspectives.add(perspectivesModel);
      }

      if (gameModel.screenshots == null) gameModel.screenshots = [];
      games.putIfAbsent(gameTableData.name, () => gameModel);

      screenshotsTableData = row.readTableOrNull(screenshotsTable);
      if (screenshotsTableData != null) {
        screenshotsModel =
            ScreenshotsModel.fromJson(screenshotsTableData.toJson());
        games[screenshotsTableData.fkName].screenshots.add(screenshotsModel);
      }
    }

    return games.values.toList();
  }
}
