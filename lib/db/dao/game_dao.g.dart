// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'game_dao.dart';

// **************************************************************************
// DaoGenerator
// **************************************************************************

mixin _$GameDaoMixin on DatabaseAccessor<AppDatabase> {
  $GameTableTable get gameTable => attachedDatabase.gameTable;
  $PlayerPerspectivesTableTable get playerPerspectivesTable =>
      attachedDatabase.playerPerspectivesTable;
  $ScreenshotsTableTable get screenshotsTable =>
      attachedDatabase.screenshotsTable;
}
