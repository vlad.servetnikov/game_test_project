import 'package:moor_flutter/moor_flutter.dart';

class ScreenshotsTable extends Table {
  TextColumn get fkName => text()();

  IntColumn get id => integer()();

  IntColumn get game => integer()();

  IntColumn get height => integer()();

  TextColumn get imageId => text()();

  TextColumn get url => text()();

  IntColumn get width => integer()();

  TextColumn get checksum => text()();

}
