import 'package:moor_flutter/moor_flutter.dart';


class PlayerPerspectivesTable extends Table {
  TextColumn get fkName => text()();

  IntColumn get id => integer()();

  IntColumn get createdAt => integer()();

  TextColumn get name => text()();

  TextColumn get slug => text()();

  IntColumn get updatedAt => integer()();

  TextColumn get url => text()();

  TextColumn get checksum => text()();

}
