import 'package:moor_flutter/moor_flutter.dart';

class GameTable extends Table {
  @override
  Set<Column> get primaryKey => {name};

  IntColumn get id => integer()();

  IntColumn get createdAt => integer()();

  IntColumn get firstRelease => integer()();

  RealColumn get popularity => real()();

  TextColumn get summary => text()();

  RealColumn get rating => real()();

  TextColumn get name => text()();

  // cover
  BoolColumn get alphaChannel => boolean()();

  BoolColumn get animated => boolean()();

  IntColumn get game => integer()();

  IntColumn get height => integer()();

  TextColumn get imageId => text()();

  TextColumn get url => text()();

  IntColumn get width => integer()();

  TextColumn get checksum => text()();
}
