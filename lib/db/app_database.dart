import 'package:game_project/db/dao/game_dao.dart';
import 'package:game_project/db/tables/player_perspectives_table.dart';
import 'package:game_project/db/tables/screenshots_table.dart';
import 'package:moor/moor.dart';
import 'package:moor_flutter/moor_flutter.dart';

import 'tables/game_table.dart';

part 'app_database.g.dart';

@UseMoor(tables: [GameTable, PlayerPerspectivesTable, ScreenshotsTable], daos: [GameDao])
class AppDatabase extends _$AppDatabase {
  AppDatabase()
      : super(FlutterQueryExecutor.inDatabaseFolder(
            path: "db.sqlite", logStatements: true));

  int get schemaVersion => 1;
}
