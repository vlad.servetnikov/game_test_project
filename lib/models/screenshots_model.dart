class ScreenshotsModel {
  int id;
  int game;
  int height;
  String imageId;
  String url;
  int width;
  String checksum;

  ScreenshotsModel(
      {this.id,
        this.game,
        this.height,
        this.imageId,
        this.url,
        this.width,
        this.checksum});

  ScreenshotsModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    game = json['game'];
    height = json['height'];
    imageId = json['image_id'];
    url = json['url'];
    width = json['width'];
    checksum = json['checksum'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['game'] = this.game;
    data['height'] = this.height;
    data['image_id'] = this.imageId;
    data['url'] = this.url;
    data['width'] = this.width;
    data['checksum'] = this.checksum;
    return data;
  }
}