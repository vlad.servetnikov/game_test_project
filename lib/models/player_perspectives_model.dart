class PlayerPerspectivesModel {
  int id;
  int createdAt;
  String name;
  String slug;
  int updatedAt;
  String url;
  String checksum;

  PlayerPerspectivesModel(
      {this.id,
        this.createdAt,
        this.name,
        this.slug,
        this.updatedAt,
        this.url,
        this.checksum});

  PlayerPerspectivesModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    createdAt = json['created_at'];
    name = json['name'];
    slug = json['slug'];
    updatedAt = json['updated_at'];
    url = json['url'];
    checksum = json['checksum'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['created_at'] = this.createdAt;
    data['name'] = this.name;
    data['slug'] = this.slug;
    data['updated_at'] = this.updatedAt;
    data['url'] = this.url;
    data['checksum'] = this.checksum;
    return data;
  }
}
