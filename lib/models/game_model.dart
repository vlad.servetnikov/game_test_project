import 'package:game_project/models/cover_model.dart';
import 'package:game_project/models/player_perspectives_model.dart';
import 'package:game_project/models/screenshots_model.dart';

class GameModel {
  final int id;
  final Cover cover;
  final int createdAt;
  final int firstRelease;
  List<PlayerPerspectivesModel> perspectives;
  final double popularity;
  List<ScreenshotsModel> screenshots;
  final String summary;
  final double rating;
  final String name;

  GameModel(
      this.id,
      this.cover,
      this.createdAt,
      this.firstRelease,
      this.perspectives,
      this.popularity,
      this.screenshots,
      this.summary,
      this.rating,
      this.name);

  GameModel.fromJson(Map<String, dynamic> json)
      : id = json["id"],
        cover = json["cover"] == null ? null : Cover.fromJson(json["cover"]),
        createdAt = json["created_at"],
        firstRelease = json["first_release_date"],
        perspectives = json["player_perspectives"] == null
            ? null
            : (json["player_perspectives"] as List)
                .map((i) => new PlayerPerspectivesModel.fromJson(i))
                .toList(),
        popularity = json["popularity"],
        screenshots = json["screenshots"] == null
            ? null
            : (json["screenshots"] as List)
                .map((i) => new ScreenshotsModel.fromJson(i))
                .toList(),
        summary = json["summary"],
        rating = json["total_rating"],
        name = json["name"];
}
