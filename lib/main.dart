import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:game_project/app/locator.dart';
import 'package:game_project/bloc/game_bloc.dart';
import 'package:game_project/service/game_repository.dart';
import 'package:game_project/ui/game_page.dart';
import 'bloc/game_event.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  setupLocator();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: BlocProvider<GameBloc>(
            create: (context) => GameBloc(gamesRepository: GamesRepository())
              ..add(FetchGames()),
            child: GamePage()));
  }
}
