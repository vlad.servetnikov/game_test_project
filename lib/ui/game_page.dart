import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:game_project/bloc/game_bloc.dart';
import 'package:game_project/bloc/game_event.dart';
import 'package:game_project/bloc/game_state.dart';
import 'package:game_project/service/game_repository.dart';
import 'package:game_project/ui/game_list_page.dart';
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';

class GamePage extends StatelessWidget {
  final gameRepository = GamesRepository();

  @override
  Widget build(BuildContext context) {
    final GameBloc gameBloc = BlocProvider.of<GameBloc>(context);
    return BlocConsumer<GameBloc, GameState>(
      bloc: gameBloc,
      listener: (a, b) {
        print('state is ${b.toString()}');
      },
      builder: (ctx, state) {
        return Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            title: Text('Game List'),
          ),
          body: LiquidPullToRefresh(
              onRefresh: () => _loadGames(gameBloc), child: GameList()),
        );
      },
    );
  }

  Future<void> _loadGames(GameBloc gameBloc) async {
    return gameBloc.add(FetchGames(offset: 0));
  }
}
