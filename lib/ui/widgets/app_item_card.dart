import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:game_project/constants/app_images.dart';
import '../../models/cover_model.dart';
import '../../models/game_model.dart';
import '../game_about_page.dart';

class AppCardGame extends StatelessWidget {
  AppCardGame({this.name, this.cover, this.summary, this.rating, this.gameModel});

  final String summary;
  final double rating;
  final String name;
  final Cover cover;
  final GameModel gameModel;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(1.0),
      child: Card(
        elevation: 3,
        child: InkWell(
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(builder: (context) {
              return (AboutGame(gameModel: gameModel));
            }));
          },
          child: Column(
            children: [
              Row(
                children: [
                  Container(
                    height: 110,
                    width: 110,
                    child: Padding(
                      padding: const EdgeInsets.all(7.0),
                      child: ClipRRect(
                          borderRadius: BorderRadius.circular(8.0),
                          child: cover != null
                              ? Image.network(
                                  'https:${cover.url}',
                                  height: 100.0,
                                  width: 100.0,
                                  fit: BoxFit.cover,
                                )
                              : Image.asset(icNoImageAvail)),
                    ),
                  ),
                  SizedBox(width: 15),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          name,
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        SizedBox(height: 10),
                        summary != null
                            ? Text(
                                summary.toString(),
                                overflow: TextOverflow.ellipsis,
                              )
                            : SizedBox(),
                      ],
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
