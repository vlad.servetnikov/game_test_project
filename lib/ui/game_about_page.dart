import 'package:flutter/material.dart';
import 'package:game_project/constants/app_images.dart';

import '../models/game_model.dart';

class AboutGame extends StatelessWidget {
  AboutGame({this.gameModel});

  final GameModel gameModel;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(
        'About Game',
        style: TextStyle(fontWeight: FontWeight.bold),
      )),
      body: ListView(
        children: [
          Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10.0),
                    child: gameModel?.cover != null
                        ? Image.network('https:${gameModel?.cover?.url}')
                        : Container(
                            height: 110, child: Image.asset(icNoImageAvail)),
                  ),
                ),
              ),
              Expanded(
                flex: 3,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      gameModel?.name,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 10),
                    gameModel?.rating != null
                        ? Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Icon(Icons.star, color: Colors.amber),
                              SizedBox(width: 3),
                              Text(
                                gameModel.rating.toString().split('.').first,
                              ),
                            ],
                          )
                        : SizedBox(),
                  ],
                ),
              ),
            ],
          ),
          Divider(height: 10, color: Colors.black),
          Padding(
              padding: const EdgeInsets.only(
                  left: 10.0, right: 10.0, top: 10.0, bottom: 10.0),
              child: gameModel.summary != null
                  ? Text(gameModel?.summary)
                  : SizedBox())
        ],
      ),
    );
  }
}
