import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:game_project/bloc/game_bloc.dart';
import 'package:game_project/bloc/game_event.dart';
import 'package:game_project/bloc/game_state.dart';
import 'package:game_project/models/game_model.dart';
import 'package:game_project/service/game_repository.dart';
import 'package:game_project/ui/widgets/app_bottom_loader.dart';
import 'package:game_project/ui/widgets/app_item_card.dart';

class GameList extends StatefulWidget {
  @override
  _GameListState createState() => _GameListState();
}

class _GameListState extends State<GameList> {
  final GamesRepository gamesRepositories = GamesRepository();
  final _scrollController = ScrollController();
  GameBloc _gameBloc;

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() => _onScroll(_gameBloc.state));
    _gameBloc = context.read<GameBloc>();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<GameBloc, GameState>(
      listener: (ctx, state) {},
      builder: (context, state) {
        switch (state?.status) {
          case GameStatus.failure:
            return ListView(
              children: [
                Center(child: Text('Failed to fetch game')),
              ],
            );
          case GameStatus.success:
            if (state.games.isEmpty) {
              return ListView(
                children: [
                  Center(
                    child: Container(
                      child: Text('No Data'),
                    ),
                  )
                ],
              );
            }
            return ListView.builder(
              itemBuilder: (BuildContext context, int index) {
                GameModel gameModel;
                if (index + 1 <= state.games.length)
                  gameModel = state.games[index];
                else
                  gameModel = null;
                return index >= state.games.length
                    ? BottomLoader()
                    : AppCardGame(
                        cover: gameModel?.cover,
                        name: gameModel?.name,
                        summary: gameModel?.summary,
                        rating: gameModel?.rating,
                        gameModel: gameModel,
                      );
              },
              itemCount: !state.withInternet
                  ? state.games.length
                  : state.hasReachedMax
                      ? state.games.length
                      : state.games.length + 1,
              controller: _scrollController,
            );
          default:
            return Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  void _onScroll(GameState state) {
    if (_isBottom)
      _gameBloc.add(FetchGames(offset: state.withInternet ? null : 0));
  }

  bool get _isBottom {
    if (_scrollController.offset >=
            _scrollController.position.maxScrollExtent &&
        !_scrollController.position.outOfRange) {
      return true;
    }
    return false;
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }
}
